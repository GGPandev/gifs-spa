import { loadTrendingGifs } from './requests/load-trending-gifs.js';
import { loadSearchedGifs } from './requests/load-searched-gifs.js';
import { MAIN_SECTION } from './common/constants.js';
import { detailedGifContainerView } from './views/detailed-gif-container-view.js';
import { trendingGifsView } from './views/trending-gifs-view.js';
import { uploadGifView } from './views/upload-gif-view.js';
import { searchGifsView } from './views/search-gifs-view.js';
import { loadGifDetails } from './requests/load-gif-details.js';
import { toggleFavoriteStatus } from './events/favorite-event.js';
import { loadFavoriteGifs } from './requests/load-favorite-gifs.js';
import { storeCurrentPage } from './data/store-current-page.js';
import { goToBackPage } from './events/backPage-events.js';
import { storeCurrentSearch } from './data/store-current-search.js';
import { uploadGif } from './data/fetching-gifs.js';
import { loadUploadedGifs } from './requests/load-uploaded-gifs.js';
import { gifBeingUploaded } from './utils/gifBeingUploaded.js';
import { gifUploadRejected } from './utils/gifUploadRejected.js';
import { storeCurrentTrendingOffset } from './data/store-current-trending-offset.js';

MAIN_SECTION.innerHTML = trendingGifsView;
loadTrendingGifs();
localStorage.setItem('backPage', 'Trending Gifs');

// Adding event-listeners to Buttons:
document.addEventListener('click', (event) => {
  // ****************************************
  if (event.target.id === 'trendingGifs-btn') {
    storeCurrentPage(event);
    MAIN_SECTION.innerHTML = trendingGifsView;
    loadTrendingGifs();
    // ****************************************
  } else if (event.target.id === 'searchGifs-btn') {
    storeCurrentPage(event);
    MAIN_SECTION.innerHTML = searchGifsView;
    document.getElementById('search-form-btn').addEventListener('click', () => {
      document.getElementById('main-container').innerHTML = '';
      const searchedText = document.getElementById('search-text').value;
      storeCurrentSearch(searchedText);
      loadSearchedGifs(searchedText);
      document.getElementById('search-text').value = '';
    });
    // ****************************************
  } else if (event.target.id === 'favorites-btn') {
    storeCurrentPage(event);
    loadFavoriteGifs();
    // ****************************************
  } else if (event.target.id === 'uploadGifs-btn') {
    MAIN_SECTION.innerHTML = uploadGifView();
    document
      .getElementById('uploadGif-form')
      .addEventListener('submit', (event) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const parsedFormData = [...formData.entries()];

        if (parsedFormData[1][1].type === 'image/gif') {
          uploadGif(formData);
          gifBeingUploaded();
        } else {
          gifUploadRejected();
        }
      });
    // ****************************************
  } else if (event.target.id === 'uploaded-btn') {
    storeCurrentPage(event);
    loadUploadedGifs();
    // ****************************************
  } else if (event.target.classList.contains('details-btn')) {
    event.preventDefault();
    MAIN_SECTION.innerHTML = detailedGifContainerView;
    const gifID = event.target.getAttribute('data-id');
    loadGifDetails(gifID);
    // ****************************************
  } else if (event.target.classList.contains('favorite')) {
    toggleFavoriteStatus(event.target.parentNode.getAttribute('data-gif-id'));
    // ****************************************
  } else if (event.target.id === 'back-btn') {
    goToBackPage();
  } else if (event.target.classList.contains('prev')) {
    // console.log(storeCurrentTrendingOffset(event.target.innerText));
    storeCurrentTrendingOffset(event.target.innerText);
    loadTrendingGifs();
  } else if (event.target.classList.contains('next')) {
    // console.log(storeCurrentTrendingOffset(event.target.innerText));
    storeCurrentTrendingOffset(event.target.innerText);
    loadTrendingGifs();
  }
});
