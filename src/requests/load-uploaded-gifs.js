import { getGifsByID } from '../data/fetching-gifs.js';
import { getUploadedGifsByIds } from '../data/store-uploaded-gifs.js';
import { displayUploadedGifs } from '../events/preDisplay-events.js';

export const loadUploadedGifs = () => {
  if (
    !localStorage.getItem('uploadedGifs') ||
    localStorage.getItem('uploadedGifs') === ''
  ) {
    document.getElementById('main-section').innerHTML = `
      <h2 id="main-section-title">Uploaded Gifs:</h2>
      <h3>Sorry, no uploaded gifs in this section!</h3>
      `;
  } else {
    const uploadedGifsArr = getUploadedGifsByIds();
    const uploadedGifsObj = getGifsByID(uploadedGifsArr);
    displayUploadedGifs(uploadedGifsObj);
  }
};
