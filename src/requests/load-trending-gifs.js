import { getTrendingGifs } from '../data/fetching-gifs.js';
import { displayTrendingGifs } from '../events/preDisplay-events.js';

export const loadTrendingGifs = () => {
  const gifs = getTrendingGifs();

  displayTrendingGifs(gifs);
};
