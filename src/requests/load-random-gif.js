import { getRandomGif } from '../data/fetching-gifs.js';
import { displayRandomGif } from '../events/preDisplay-events';

export const loadRandomGif = () => {
  const randomGif = getRandomGif();

  displayRandomGif(randomGif);
};
