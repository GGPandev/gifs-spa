import { getSearchedGifs } from '../data/fetching-gifs.js';
import { displaySearchedGifs } from '../events/preDisplay-events.js';

export const loadSearchedGifs = (searchedText) => {
  const gifs = getSearchedGifs(searchedText);

  displaySearchedGifs(gifs);
};
