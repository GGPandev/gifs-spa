import { getFavorites } from '../data/favorite-gifs.js';
import { getGifsByID, getRandomGif } from '../data/fetching-gifs.js';
import {
  displayFavoriteGifs,
  displayRandomGif,
} from '../events/preDisplay-events.js';

export const loadFavoriteGifs = () => {
  if (
    !localStorage.getItem('favorites') ||
    localStorage.getItem('favorites') === '[]'
  ) {
    const awaitedRandomGif = getRandomGif();
    displayRandomGif(awaitedRandomGif);
  } else {
    const favoriteGifsFromLocalStorage = getFavorites();
    const favoriteGifs = getGifsByID(favoriteGifsFromLocalStorage);
    displayFavoriteGifs(favoriteGifs);
  }
};
