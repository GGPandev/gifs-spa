import { getGifByID } from '../data/fetching-gifs.js';
import { displayGifDetails } from '../events/preDisplay-events.js';

export const loadGifDetails = (gifID) => {
  const searchedGif = getGifByID(gifID);

  displayGifDetails(searchedGif);
};
