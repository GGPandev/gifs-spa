import { TRENDING_LIMIT } from '../common/constants.js';

export const storeCurrentTrendingOffset = (button) => {
  let currOffset = Number(localStorage.getItem('trendingOffset') || 0);
  const trendingTotalCount = localStorage.getItem('trendingTotalCount');

  if (button === 'Next') {
    if (currOffset + TRENDING_LIMIT > trendingTotalCount) {
      return currOffset;
    }
    currOffset += TRENDING_LIMIT;
  } else if (button === 'Prev') {
    if (currOffset - TRENDING_LIMIT < 0) {
      currOffset = 0;
    } else {
      currOffset -= TRENDING_LIMIT;
    }
  }

  localStorage.setItem('trendingOffset', currOffset);
  return localStorage.getItem('trendingOffset');
};
