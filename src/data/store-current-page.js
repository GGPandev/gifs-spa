export const storeCurrentPage = (event) => {
  const selected = event.target.innerText;
  localStorage.setItem('backPage', selected);
};
