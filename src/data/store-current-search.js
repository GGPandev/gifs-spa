export const storeCurrentSearch = (searchedText) => {
  localStorage.setItem('lastSearch', searchedText);
};
