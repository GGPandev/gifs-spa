export const storeUploadedGif = (uploadedGif) => {
  const uploadedGifs = localStorage.getItem('uploadedGifs') || null;
  let allUploadedGifs;

  if (uploadedGifs === null) {
    allUploadedGifs = [uploadedGif];
  } else {
    allUploadedGifs = [uploadedGifs, uploadedGif];
  }
  localStorage.setItem('uploadedGifs', allUploadedGifs);
};

export const getUploadedGifsByIds = () => {
  return localStorage.getItem('uploadedGifs').split(',');
};
