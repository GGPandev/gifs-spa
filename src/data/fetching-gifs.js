import { APIKEY, SEARCH_LIMIT, TRENDING_LIMIT } from '../common/constants.js';
import { gifBeingUploaded, gifUploaded } from '../utils/gifBeingUploaded.js';
import { storeUploadedGif } from './store-uploaded-gifs.js';

// Get Trending Gifs: ----------------------------------------------------------
export const getTrendingGifs = () => {
  const trendingOffset = localStorage.getItem('trendingOffset');
  const url = `https://api.giphy.com/v1/gifs/trending?api_key=${APIKEY}&limit=${TRENDING_LIMIT}&offset=${trendingOffset}`;

  return fetch(url).then((data) => data.json());
};

// Get Searched Gifs: ----------------------------------------------------------
export const getSearchedGifs = (searchedText) => {
  const url = `https://api.giphy.com/v1/gifs/search?api_key=${APIKEY}&q=${searchedText}&limit=${SEARCH_LIMIT}`;

  return fetch(url).then((data) => data.json());
};

// Get Gif by ID: --------------------------------------------------------------
export const getGifByID = (gifID) => {
  const url = `https://api.giphy.com/v1/gifs/${gifID}?api_key=${APIKEY}`;

  return fetch(url).then((data) => data.json());
};

// Get Gifs by ID: -------------------------------------------------------------
export const getGifsByID = (gifsIDs) => {
  const gifsIDsString = gifsIDs.join(',');
  // console.log(gifsIDsString);
  const url = `https://api.giphy.com/v1/gifs?api_key=${APIKEY}&ids=${gifsIDsString}`;

  return fetch(url).then((data) => data.json());
};

// Get Random Gifs: ------------------------------------------------------------
export const getRandomGif = () => {
  const url = `https://api.giphy.com/v1/gifs/random?api_key=${APIKEY}`;

  return fetch(url).then((data) => data.json());
};

// Upload Gifs using file: -----------------------------------------------------
export const uploadGif = async (formData) => {
  const url = `https://upload.giphy.com/v1/gifs?api_key=${APIKEY}`;

  gifBeingUploaded();

  const resolved = await fetch(url, {
    method: 'POST',
    body: formData,
  })
      .then((res) => res.json())
      .catch((err) => console.log(err));

  if (resolved) {
    storeUploadedGif(resolved.data.id);
    gifUploaded();
  }
};

// Using URL:
// =============================================================================
// return fetch(url, {
//   method: 'POST',
//   headers: {
//     'Content-Type': 'application/x-www-form-urlencoded',
//   },
//   body: new URLSearchParams({
//     source_image_url: uploadGifUrl,
//     tags: tags,
//     title: uploadGifTitle,
//   }),
// })
//   .then((res) => res.json())
//   .catch((err) => console.log(err));
// };
// =============================================================================
