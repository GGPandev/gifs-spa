import { searchGifsView } from '../views/search-gifs-view.js';
import { MAIN_SECTION } from '../common/constants.js';
import { loadTrendingGifs } from '../requests/load-trending-gifs.js';
import { trendingGifsView } from '../views/trending-gifs-view.js';
import { loadFavoriteGifs } from '../requests/load-favorite-gifs.js';
import { loadSearchedGifs } from '../requests/load-searched-gifs.js';
import { storeCurrentSearch } from '../data/store-current-search.js';
import { loadUploadedGifs } from '../requests/load-uploaded-gifs.js';

export const goToBackPage = () => {
  const lastClicked = localStorage.getItem('backPage');
  if (lastClicked === 'Trending Gifs') {
    MAIN_SECTION.innerHTML = trendingGifsView;
    loadTrendingGifs();
  } else if (lastClicked === 'Favorite Gifs') {
    loadFavoriteGifs();
  } else if (lastClicked === 'Search Gifs') {
    MAIN_SECTION.innerHTML = searchGifsView;
    loadSearchedGifs(localStorage.getItem('lastSearch'));
    document.getElementById('search-form-btn').addEventListener('click', () => {
      document.getElementById('main-container').innerHTML = '';
      const searchedText = document.getElementById('search-text').value;
      storeCurrentSearch(searchedText);
      loadSearchedGifs(searchedText);
      document.getElementById('search-text').value = '';
    });
  } else if (lastClicked === 'Your uploads') {
    loadUploadedGifs();
  }
};
