import { detailedGifView } from '../views/detailed-gif-view.js';
import { uploadedGifsView } from '../views/uploaded-gifs-view.js';
import { cardGifView } from '../views/card-gif-view.js';
import { favoriteGifsView } from '../views/favorite-gifs-view.js';
import { calcCurrentTrendingPage } from '../utils/calcCurrentTrendingPage.js';

export const displaySearchedGifs = async (gifs) => {
  const resolved = await gifs;
  resolved.data.forEach((gif) => {
    document.getElementById('main-container').innerHTML += cardGifView(gif);
  });
};

export const displayTrendingGifs = async (gifs) => {
  const trendingGifs = await gifs;

  const trendingTotalCount = trendingGifs.pagination.total_count;
  localStorage.setItem('trendingTotalCount', trendingTotalCount);

  document.getElementById('main-container').innerHTML = '';
  trendingGifs.data.forEach((gif) => {
    document.getElementById('main-container').innerHTML += cardGifView(gif);
  });

  document.getElementById(
      'current-trading-page'
  ).innerText = `- ${calcCurrentTrendingPage()} -`;
};

export const displayGifDetails = async (promisedGif) => {
  const detailedGif = await promisedGif;

  document.getElementById('detailed-container').innerHTML = detailedGifView(
      detailedGif.data
  );
};

export const displayFavoriteGifs = async (favoriteGifsIDs) => {
  const favoriteGifsObj = await favoriteGifsIDs;

  document.getElementById('main-section').innerHTML = favoriteGifsView(
      favoriteGifsObj.data
  );
};

export const displayRandomGif = async (randomGif) => {
  const resRandomGif = await randomGif;
  document.getElementById('main-section').innerHTML = favoriteGifsView([
    resRandomGif.data,
  ]);
};

export const displayUploadedGifs = async (uploadedGifsObj) => {
  const resUploadedGifs = await uploadedGifsObj;

  document.getElementById('main-section').innerHTML = uploadedGifsView(
      resUploadedGifs.data
  );
};
