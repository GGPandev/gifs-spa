import {
  addFavorite,
  getFavorites,
  removeFavorite,
} from '../data/favorite-gifs.js';

export const toggleFavoriteStatus = (gifId) => {
  const favorites = getFavorites();
  const currDiv = document.querySelector(`div[data-gif-id="${gifId}"]`);

  if (favorites.includes(gifId)) {
    removeFavorite(gifId);
  } else {
    addFavorite(gifId);
  }

  if (currDiv.children[0].classList.contains('hidden')) {
    currDiv.children[0].classList.remove('hidden');
    currDiv.children[1].classList.add('hidden');
  } else {
    currDiv.children[0].classList.add('hidden');
    currDiv.children[1].classList.remove('hidden');
  }
};
