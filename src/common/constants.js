// export const APIKEY = 'bFpYP6zwCJ0qOAdPi6xQUfyxk6IAiZfT'; // Georgi
export const APIKEY = 'iRWHgsaq7O5VC2CEKlYCzQBqIFrZPTTS'; // Costa

export const TRENDING_LIMIT = 8;
export const SEARCH_LIMIT = 20;

// Sections:
export const MAIN_SECTION = document.getElementById('main-section');
export const STATUS_SECTION = document.getElementById('status-msg-section');

// Section Titles:
export const MAIN_SECTION_TITLE = document.getElementById('main-section-title');
export const STATUS_TITLE = document.getElementById('status-title');
export const STATUS_MSG = document.getElementById('status-msg');

// Containers:
export const MAIN_CONTAINER = document.getElementById('main-container');
export const SEARCH_GIFS_CONTAINER = document.getElementById(
  'searchGifs-container'
);

export const FULL_STAR = `★`;

export const EMPTY_STAR = `☆`;
