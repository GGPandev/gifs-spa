export const checkIfUserFieldExists = (currentGifObj) => {
  if (!currentGifObj.user || !currentGifObj.user.description) {
    return 'N/A for this GIF!';
  }

  return currentGifObj.user.description;
};
