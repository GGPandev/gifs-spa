import { MAIN_SECTION, STATUS_MSG, STATUS_TITLE } from '../common/constants.js';
import { loadUploadedGifs } from '../requests/load-uploaded-gifs.js';

export const gifBeingUploaded = () => {
  const title = 'Upload Gifs';
  const message = 'Your GIF is Being Uploaded...';

  MAIN_SECTION.innerHTML = '';
  STATUS_TITLE.textContent = title;
  STATUS_MSG.textContent = message;

  STATUS_TITLE.classList.remove('hidden');
  STATUS_MSG.classList.remove('hidden');
};

export const gifUploaded = () => {
  STATUS_TITLE.classList.add('hidden');
  STATUS_MSG.classList.add('hidden');
  loadUploadedGifs();
};
