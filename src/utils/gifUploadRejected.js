export const gifUploadRejected = () => {
  const errorMsg = document.getElementById('upload-error-msg');
  errorMsg.classList.remove('hidden');

  setTimeout(() => {
    errorMsg.classList.add('hidden');
  }, 3000);
};
