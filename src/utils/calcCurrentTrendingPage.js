import { TRENDING_LIMIT } from '../common/constants.js';

export const calcCurrentTrendingPage = () => {
  return Number(localStorage.getItem('trendingOffset') / TRENDING_LIMIT + 1);
};
