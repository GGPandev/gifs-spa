import { cardGifView } from './card-gif-view.js';

export const uploadedGifsView = (uploadedGifsObj) => {
  return `
    <h2>Uploaded Gifs:</h2>
    <div id="main-container">
      ${
  uploadedGifsObj.map(cardGifView).join('\n') ||
        '<p>Upload some Gifs to see them here.</p>'
}
    </div>
  `;
};
