export const uploadGifView = () => {
  return `
  <h2>Upload Gifs:</h2>
  <form id="uploadGif-form" method="post">
    <label for="uploadGif-title">Title:</label>
    <input type="text" name="title" id="uploadGif-title" autocomplete="off"/>
  
    <input
      type="file"
      name="file"
      accept="image"
      id="uploadGif-url"
      class="select-file-btn"
      placeholder="Place the URL of your GIF here..."
    />
    <p id="upload-error-msg" class="hidden">
      Wrong File Extension! Please choose a valid GIF file!!!
    </p>
  
    <label for="uploadGif-tags">Gif Tags:</label>
    <textarea
      name="tags"
      id="uploadGif-tags"
      cols="30"
      rows="5"
      placeholder="Type your Gif tags here..."
    ></textarea>
  
    <div>
    <button class="btn button">Upload Gif!</button>
    <button class="btn button" type="reset">Clear Form</button>
    </div>
  </form>
  
  `;
};
