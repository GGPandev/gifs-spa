import { cardGifView } from './card-gif-view.js';
export const favoriteGifsView = (favoriteGifs) => {
  return `
    <h2>Favorite Gifs:</h2>
    <div id="main-container">
      ${
  favoriteGifs.map(cardGifView).join('\n') ||
        '<p>Add some Gifs to favorites to see them here.</p>'
}
    </div>
  `;
};
