import { checkIfUserFieldExists } from '../utils/check-if-user-field-exists.js';

export const detailedGifView = (gif) => {
  const title = gif.title.split('GIF');

  return `
  <div class="card mb-3" style="max-width: 100%">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="${gif.images.original.url}" class="card-img" alt="..." />
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title"><strong>${title[0].trim()}</strong></h5>
        <p class="card-text"><strong>Description: </strong> 
        ${checkIfUserFieldExists(gif)}
        </p>
        <p class="card-text card-upload-date">
          <small class="text-muted"><strong>Date of upload:</strong> ${
  gif.import_datetime
}</small>
        </p>
        <button class="button btn" id="back-btn">Back</button>
      </div>
    </div>
  </div>
</div>
`;
};
