import { EMPTY_STAR, FULL_STAR } from '../common/constants.js';
import { getFavorites } from '../data/favorite-gifs.js';

export const cardGifView = (gif) => {
  const title = gif.title.split('GIF');

  const favorites = getFavorites();

  return `
  <div class="my-card">
  <img
    class="card-img-top"
    src="${gif.images.fixed_width.url}"
    alt="Card image cap"
  />
  <div class="card-body">
    <h5 class="card-title">${title[0].trim()}</h5>
    <p class="card-text">${gif.import_datetime}</p>
    <div class="buttons-wrapper">
      <a href="#" class="btn btn-primary details-btn" data-id="${
        gif.id
      }">Details</a>
      <div class="favorite-wrapper" data-gif-id="${gif.id}">
        <div class="favorite">
          ${favorites.includes(gif.id) ? FULL_STAR : EMPTY_STAR}
        </div>
        <div class="favorite hidden">
          ${favorites.includes(gif.id) ? EMPTY_STAR : FULL_STAR}
        </div>
      </div>
    </div>
  </div>
</div>

`;
};
