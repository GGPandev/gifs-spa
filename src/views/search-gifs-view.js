export const searchGifsView = `
<h2>Search Gifs:</h2>
<div id="search-form">
  <label for="search-text">What are you looking for?</label>
  <input type="text" name="search-text" id="search-text" autocomplete="off"/>
  <button type="button" id="search-form-btn" class="button btn">Search</button>
</div>
<div id="main-container">
</div>
`;
