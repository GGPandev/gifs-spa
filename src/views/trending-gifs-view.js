export const trendingGifsView = `
<h2 id="main-section-title">Trending Gifs:</h2>
<div id="main-container"></div>
<div id="pagination-btns" style="display: flex; justify-content: center">
  <button class="prev btn">Prev</button>
  <p id="current-trading-page"></p>
  <button class="next btn">Next</button>
</div>
`;
